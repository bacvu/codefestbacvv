// Precondition: You need to require socket.io.js in your html page
// Reference link https://socket.io
// <script src="socket.io.js"></script>
const gameId = '0d34d993-b7f2-4bb5-ad6e-dbe6f0db433d';
const playerId = 'player1-xxx';

// Connecto to API App server
const apiServer = 'https://codefest.techover.io';
const socket = io.connect(apiServer, { reconnect: true, transports: ['websocket'] });

// Value map
const emptyBox = 0;
const stoneWall = 1;
const woodenWall = 2;

var MAP_y = 18;
var MAP_x =  28;

//Type Spoils
const SPACE_STONE = 3;
const MIND_STONE = 4;
const REALITY_STONE = 5;
const POWER_STONE = 6;
const TIME_STONE = 7;
const SOUL_STONE = 8;

//Tag
var TICKTACK_TAG = {
    PLAYER_START_MOVING: 'player:start-moving',
    PLAYER_STOP_MOVING: 'player:stop-moving',
    PLAYER_PICK_SPOIL: 'player:pick-spoil',
    PLAYER_BANNED: 'player:moving-banned',
    BOMB_SETUP: 'bomb:setup',
    BOMB_EXPLOSED: 'bomb:explosed',
    START_GAME: 'start-game'
}

// LISTEN SOCKET.IO EVENTS

// It it required to emit `join channel` event every time connection is happened
socket.on('connect', () => {
    document.getElementById('connected-status').innerHTML = 'ON';
    document.getElementById('socket-status').innerHTML = 'Connected';
    // console.log('[Socket] connected to server');    
    // API-1a
    socket.emit('join game', { game_id: gameId, player_id: playerId });

});

socket.on('disconnect', () => {
    // console.warn('[Socket] disconnected');
    document.getElementById('socket-status').innerHTML = 'Disconnected';
});

socket.on('connect_failed', () => {
    // console.warn('[Socket] connect_failed');
    document.getElementById('socket-status').innerHTML = 'Connected Failed';
});


socket.on('error', (err) => {
    // console.error('[Socket] error ', err);
    document.getElementById('socket-status').innerHTML = 'Error!';
});


// SOCKET EVENTS

// API-1b
socket.on('join game', (res) => {
    // console.log('[Socket] join-game responsed', res);
    document.getElementById('joingame-status').innerHTML = 'ON';
});

var responIgnor = 0;
var locMoc = {row: 0, col: 0}
var arrMap;
var listMoved = [];

//API-2
socket.on('ticktack player', (res) => {
    // console.info('ticktack');
    arrMap = res.map_info;
    if(arrMap != null)
    {
        GetSizeMap(res);
    }
    // console.log("Res", res);
    // console.log('[Socket] ticktack-player responsed, map_info: ', res.map_info);
    ChienThuatBacVV(res);
    //var str = DatBomBacVV(arrMap);
    // var dt = GetLocationDoiThu(arrMap);
    // var test = RunWhenAttack(arrMap, dt);
});

// API-3a
//socket.emit('drive player', { direction: str });
//API-3b
socket.on('drive player', (res) => {
    if(res.player_id == playerId)
    {
        listMoved.push(res.direction);
    }
    console.log('[Socket] drive-player responsed, res: ', res);
});

//Slepp 
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function Move(str) {
    console.log('Handle when not movie:', str);
    socket.emit('drive player', { direction: str });
}

function GetSizeMap(res) {
    var map = res.map_info.size;
    MAP_y = map.rows;
    MAP_x = map.cols;
}





