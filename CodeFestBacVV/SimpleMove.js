function IsMoveUp(arr, y, x) {
	// body...
	//Inorge location
	let ok = 0;
	listLocationIgnore.forEach(item => {
		if(item.row == (y - 1) && item.col == x) {ok++;}
	});

	var map1 = arr.map;
	if(map1[y-1][x] == emptyBox && ok == 0) return true;
	return false;
}

function IsMoveDown(arr, y, x) {
	// body...
	//Inorge location
	let ok = 0;
	listLocationIgnore.forEach(item => {
		if(item.row == (y + 1) && item.col == x){
			ok++;
		}
	});
	
	var map1 = arr.map;
	if(map1[y+1][x] == emptyBox && ok == 0) return true;
	return false;
}

function IsMoveLeft(arr, y, x) {
	// body...
	//Inorge location
	let ok = 0;
	listLocationIgnore.forEach(item => {
		if(item.row == y && item.col == (x - 1))
		{
			ok++;
		}
	});

	var map1 = arr.map;
	if(map1[y][x - 1] == emptyBox && ok == 0) return true;
	return false;
}

function IsMoveRight(arr, y, x) {
	// body...
	//Inorge location
	let ok = 0;
	listLocationIgnore.forEach(item => {
		if(item.row == y && item.col == (x + 1)) {ok++;}
	});
	
	var map1 = arr.map;
	if(map1[y][x + 1] == emptyBox && ok == 0) return true;
	return false;
}

function SimpleMoveBacVV(arr, loc) {
	// body...
	var up = SimpleMoveUp(arr, loc);
	var down = SimpleMoveDown(arr, loc);
    
	var move = up.length > down.length ? up : down;
	console.log("SIMPLE MOVE");
	socket.emit('drive player', { direction: move });
    return move;
}

//Move up
function SimpleMoveUp(arr, loc) {
	// body...
	var exist = 0;
	var str = "";
	var x = loc.col;
	var y = loc.row;
    var map1 = arr.map;
	while((IsMoveUp(arr, y, x) || IsMoveRight(arr, y, x) || IsMoveLeft(arr, y, x)) && y > 0 && str.length < 8)
	{
         if(IsMoveUp(arr, y, x))
         {
         	y--;
         	str += '3';
         }
         else {
         	var l = 0;
         	var r = 0;
         	for (var i = 1; i <= 5; i++) {
         		if((x + i) < 27)
         		{
         			if(map1[y][x + i] == emptyBox)
	         		{
	         			r += 3;
	         		}
	         		else if(map1[y][x+i] == woodenWall){
	                    r += 1;
	         		}
	         		else if(map1[y][x+i] == stoneWall){
	                     var h = 6 - i;
	                     r -= 2*h;
	         		}
         		}

         		if((x - i) > 0)
         		{
         			if(map1[y][x-i] == emptyBox)
	         		{
	         			l += 3;
	         		}
	         		else if(map1[y][x-i] == woodenWall){
	                    l += 1;
	         		}
	         		else if(map1[y][x-i] == stoneWall){
	                     var h = 6 - i;
	                     l -= 2*h;
	         		}
         		}
         	}

         	if(IsMoveLeft(arr, y, x) && IsMoveRight(arr, y, x))
         	{
				if(l > r)
         		{
					if(str[str.length - 1] == '2') break;
					str += '1';
					x--;
				}
				else{
					if(str[str.length - 1] == '1') break;
					str += '2';
					x++;
				}
         	}
         	else if((IsMoveLeft(arr, y, x) == false) && IsMoveRight(arr, y, x)){
         		if(str[str.length - 1] == '1') break;
                 str += '2';
                 x++;
			 }
			 else{
				 if(IsMoveLeft(arr, y, x))
				 {
					if(str[str.length - 1] == '2') break;
					str += '1';
					x--;
				 }
			 }
         }
         exist++;
         if(exist > 50) break;
     }
    
     return str;
}

//Move Down
function SimpleMoveDown(arr, loc) {
	// body...
	var exist = 0;
	var str = "";
	var x = loc.col;
	var y = loc.row;
    var map1 = arr.map;
	while((IsMoveDown(arr, y, x) || IsMoveRight(arr, y, x) || IsMoveLeft(arr, y, x)) && y < 18 && str.length < 8)
	{
         if(IsMoveDown(arr, y, x))
         {
         	y++;
         	str += '4';
         }
         else {
         	var l = 0;
         	var r = 0;
         	for (var i = 1; i <= 5; i++) {
         		if((x + i) < 27)
         		{
         			if(map1[y][x + i] == emptyBox)
	         		{
	         			r += 3;
	         		}
	         		else if(map1[y][x+i] == woodenWall){
	                    r += 1;
	         		}
	         		else if(map1[y][x+i] == stoneWall){
	                     var h = 6 - i;
	                     r -= 2*h;
	         		}
         		}

         		if((x - i) > 0)
         		{
         			if(map1[y][x-i] == emptyBox)
	         		{
	         			l += 3;
	         		}
	         		else if(map1[y][x-i] == woodenWall){
	                    l += 1;
	         		}
	         		else if(map1[y][x-i] == stoneWall){
	                     var h = 6 - i;
	                     l -= 2*h;
	         		}
         		}
         	}

         	if(IsMoveLeft(arr, y, x) && IsMoveRight(arr, y, x))
         	{
				if(l > r)
         		{
					if(str[str.length - 1] == '2') break;
					str += '1';
					x--;
				}
				else{
					if(str[str.length - 1] == '1') break;
					str += '2';
					x++;
				}
         	}
         	else if((IsMoveLeft(arr, y, x) == false) && IsMoveRight(arr, y, x)){
         		if(str[str.length - 1] == '1') break;
                 str += '2';
                 x++;
			 }
			 else{
				 if(IsMoveLeft(arr, y, x))
				 {
					if(str[str.length - 1] == '2') break;
					str += '1';
					x--;
				 }
			 }
         }

         exist++;
         if(exist > 50) break;
     }
     return str;
}

function CheckNotMove(arr, str) {
	var myl = GetMylocation(arr);
	var y = myl.row;
	var x = myl.col;
	for (let i = 0; i < str.length; i++) {
		var str1 = str[i];
		switch (str1) {
			case '1':
				if(IsMoveLeft(arr, y, x) == false)
				{
					return true;
				}
				x--;
				break;
			case '2':
				if(IsMoveRight(arr, y, x) == false)
				{
					return true;
				}
				x++;
				break;
			case '3':
				if(IsMoveUp(arr, y, x) == false)
				{
					return true;
				}
				y--;
				break;
			case '4':
				if(IsMoveDown(arr, y, x) == false)
				{
					return true;
				}
				y++;
				break;        
			default:
				break;
		}
		
	}
	return false;
}