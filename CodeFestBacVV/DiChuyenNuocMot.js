
function FindDuongDenTuongGo(arr) {
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var map1 = arr.map;
    var listTarget = [];
    var up = FindLocationUP(map1, y, x);
    var down = FindLocationDown(map1, y, x);
    var left = FindLocationLeft(map1, y, x);
    var right = FindLocationRight(map1, y, x);
    if(up != null) listTarget.push(up);
    if(down != null) listTarget.push(down);
    if(left != null) listTarget.push(left);
    if(right != null) listTarget.push(right);

    var listMove = [];
    for (let i = 0; i < listTarget.length; i++) {
        var str = MoveToTarget(arr, listTarget[i]);
        if(str.length > 0)
        {
            listMove.push(str);
        }
    }

    if(listMove.length == 0) return "";
    var result = listMove[0];
    for (let j = 0; j < listMove.length; j++) {
        if(result.length > listMove[j].length)
        {
            result = listMove[j];
        }
        
    }
    return result;
}

function FindLocationLeft(map1, y, x) {
    var loc = null;
     //Find location gan nhat là tường gạch phía trái trên;
     while(x > 0)
     {
         if(map1[y][x-1] != woodenWall)
         {
             x--;
         }
         else if(map1[y][x-1] == woodenWall) 
         {
            loc = {row: y, col: x};
            break;
         }
     }
     return loc;
}

function FindLocationRight(map1, y, x) {
    var loc = null;
     //Find location gan nhat là tường gạch phía bên phải;
     while(x < (MAP_x - 1))
     {
         if(map1[y][x+1] != woodenWall)
         {
             x++;
         }
         else if(map1[y][x+1] == woodenWall) 
         {
            loc = {row: y, col: x};
            break;
         }
     }
     return loc;
}
function FindLocationUP(map1, y, x) {
    var loc = null;
     //Find location gan nhat là tường gạch phía bên trên;
     while(y > 0)
     {
         if(map1[y-1][x] != woodenWall)
         {
             y--;
         }
         else if(map1[y-1][x] == woodenWall) 
         {
            loc = {row: y, col: x};
            break;
         }
     }
     return loc;
}
function FindLocationDown(map1, y, x) {
    var loc = null;
     //Find location gan nhat là tường gạch phía bên trên;
     while(y < (MAP_y - 1))
     {
         if(map1[y+1][x] != woodenWall)
         {
             y++;
         }
         else if(map1[y+1][x] == woodenWall) 
         {
            loc = {row: y, col: x};
            break;
         }
     }
     return loc;
}