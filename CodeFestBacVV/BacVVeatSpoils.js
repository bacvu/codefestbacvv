function GetListSpoils(arr, listSpoils, loc) {

    //var y = loc.row;
    //var x = loc.col;
    var listStone = [];
    var point = 0;
    //var listPlayer = arr.players;
    var speed = 100;
    speed = GetMySpeed(arr);
    for (var i = 0; i < listSpoils.length; i++) {
        point = 0;
       var stone = listSpoils[i];
       var xStone = stone.col;
       var yStone = stone.row;
       var type = stone.spoil_type;
       var isCorner = CheckCorner(arr);
       point += GetPointFollowType(type, speed, isCorner);
       point += GetPointFollowDic(loc, yStone, xStone);
       var daquy = {point:point, row: yStone, col: xStone, type: type};
       listStone.push(daquy);
   }
   return listStone;
}

function GetPointFollowType(type, speed, isCorner) {
    switch (type) {
        case SOUL_STONE:
            return 150;
        case MIND_STONE:
            if(speed >= 150) return 70;
            return -150;
        case SPACE_STONE:
            if(isCorner) return -150;
            return -150;
        case REALITY_STONE:
            if(speed <= 150)
            {
                return 150;
            }
            return 50;
       case TIME_STONE:
            return 50;
       case POWER_STONE:
            return 50;            
        default:
            return 0;
    }
}

function CheckCorner(arr)
{
    //check goc trên bên trái;
    var locDt = GetLocationDoiThu(arr);
    var x = locDt.col;
    var y = locDt.row;
    if(x > 0 && x < (MAP_x / 3) && y > 0 && y < 5)
    {
        return true;
    }
    else if(x > 18 && x < (MAP_y / 3) && y > 0 && y < 5)
    {
        return true;
    }
    else if(x > 0 && x < (MAP_x / 3) && y > (MAP_y - 5) && y < MAP_y)
    {
        return true;
    }
    else if(x > (MAP_x - 5) && x < MAP_x && y > (MAP_y - 5) && y < MAP_y)
    {
        return true;
    }

    return false;
}

function GetPointFollowDic(loc, y, x)
{
     var dy = Math.abs(loc.row - y);
     var dx = Math.abs(loc.col - x);
     var point = -200;
     if(dx <= 4 && dy <= 4)
     {
         point = (10 - dx - dy) * 8;
     }
     return point;
}

var listLocationIgnore = [];

function eatspoils(arr, location) {
    // body...
    var listSpoils = arr.spoils;
    var listStone = GetListSpoils(arr, listSpoils, location);
    if(listStone.length == 0) return false;
    var stone = listStone[0];
    var targetLocation = {row: stone.row, col: stone.col, type: stone.type};
    listLocationIgnore = [];
    for (let i = 0; i < listStone.length; i++) {
        if(stone.point < listStone[i].point)
        {
            stone = listStone[i];
            targetLocation = {row: stone.row, col: stone.col, type: stone.type};

        }
        if(listStone[i].point < 0){
            var locationIgnore = {row: listSpoils[i].row, col: listSpoils[i].col};
            listLocationIgnore.push(locationIgnore);
        }
    }
    if(stone.point < 0) return false;
    var moveEatSpoils = ""
    var listMove = [];
    location = GetMylocation(arr);
    var up = moveUpWithTarget(arr, location, targetLocation);
    var down = moveDownWithTarget(arr, location, targetLocation);
    var right = moveRightWithTarget(arr, location, targetLocation);
    var left = moveLeftWithTarget(arr, location, targetLocation);
    
    for (let i = 0; i < up.length; i++) {
        var loc = up[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < down.length; i++) {
        var loc = down[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < right.length; i++) {
        var loc = right[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < left.length; i++) {
        var loc = left[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    if(listMove.length == 0) return false;
    moveEatSpoils = listMove[0];
    for (let i = 0; i < listMove.length; i++) {
        if(moveEatSpoils.length > listMove[i].length)
        {
            moveEatSpoils = listMove[i].length;
        }
    }
    if(moveEatSpoils.length > 0)
    {
        //console.log("The Way to eat Spoils:", moveEatSpoils);
        socket.emit('drive player', { direction: moveEatSpoils });
        //await sleep(3000);
        return true;
    }
    return false;
}

function MoveToTarget(arr, targetLocation) {
    var moveEatSpoils = ""
    var listMove = [];
    var location = GetMylocation(arr);
    var up = moveUpWithTarget(arr, location, targetLocation);
    var down = moveDownWithTarget(arr, location, targetLocation);
    var right = moveRightWithTarget(arr, location, targetLocation);
    var left = moveLeftWithTarget(arr, location, targetLocation);
    
    for (let i = 0; i < up.length; i++) {
        var loc = up[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < down.length; i++) {
        var loc = down[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < right.length; i++) {
        var loc = right[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < left.length; i++) {
        var loc = left[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    if(listMove.length == 0) return "";
    moveEatSpoils = listMove[0];
    for (let i = 0; i < listMove.length; i++) {
        if(moveEatSpoils.length > listMove[i].length)
        {
            moveEatSpoils = listMove[i].length;
        }
    }
    return moveEatSpoils;
}

function moveUpWithTarget(arr,location, locationTarget) {
    // body...
    var listPointMove = [];
    var str = "";
    var map1 = arr.map;
    var x = location.col;
    var y = location.row;

    var loc = {row: y, col: x, way:""};
    var exist = 0;
    while(y >= locationTarget.row)
    {
        if(IsMoveUp(arr, y, x) && y > locationTarget.row)
        {
            str += '3';
            y--; 
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveLeft(arr, y, x) && x > locationTarget.col)
        {
            if(str[str.length - 1] == '2') break;
            str += '1';
            x--;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveRight(arr, y, x)){
            if(str[str.length - 1] == '1') break;
            str += '2';
            x++;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        exist++;
        if(exist >= 50) break;
    }
    return listPointMove;
}

function moveDownWithTarget(arr, location, locationTarget) {
    // body...
    var listPointMove = [];
    var str = "";
    var map1 = arr.map;
    var x = location.col;
    var y = location.row;
    var loc = {row: y, col: x, way: str};
    var exist = 0;
    while(y <= locationTarget.row)
    {
        if(IsMoveDown(arr, y, x) && y < locationTarget.row)
        {
            str += '4';
            y++; 
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveLeft(arr, y, x) && x > locationTarget.col)
        {
            if(str[str.length - 1] == '2') break;
            str += '1';
            x--;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveRight(arr, y, x)){
            if(str[str.length - 1] == '1') break;
            str += '2';
            x++;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }

        exist++;
        if(exist >= 50) break;
    }
    return listPointMove;
}

function moveRightWithTarget(arr, location, locationTarget) {
    // body...
    var listPointMove = [];
    var str = "";
    var map1 = arr.map;
    var x = location.col;
    var y = location.row;
    var loc = {row: y, col: x, way: str};
    var exist = 0;
    while(x <= locationTarget.col)
    {
        if(IsMoveRight(arr, y, x) && x < locationTarget.col)
        {
            str += '2';
            x++; 
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveUp(arr, y, x) && y > locationTarget.row)
        {
            if(str[str.length - 1] == '4') break;
            str += '3';
            y--;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveDown(arr, y, x)){
            if(str[str.length - 1] == '3') break;
            str += '4';
            y++;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        exist++;
        if(exist >= 50) break;
    }
    return listPointMove;
}

function moveLeftWithTarget(arr, location, locationTarget) {
    // body...
    var listPointMove = [];
    var str = "";
    var map1 = arr.map;
    var x = location.col;
    var y = location.row;
    var loc = {row: y, col: x, way: str};
    var exist = 0;
    while(x >= locationTarget.col)
    {
        if(IsMoveLeft(arr, y, x) && x > locationTarget.col)
        {
            str += '1';
            x--; 
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveUp(arr, y, x) && y > locationTarget.row)
        {
            if(str[str.length - 1] == '4') break;
            str += '3';
            y--;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else if(IsMoveDown(arr, y, x)){
            if(str[str.length - 1] == '3') break;
            str += '4';
            y++;
            loc = {row: y, col: x, way: str};
            listPointMove.push(loc);
        }
        else{
            break;
        }
        exist++;
        if(exist >= 50) break;
    }
    return listPointMove; 
}