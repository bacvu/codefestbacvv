

function FindListTarget(arr, r) {
    var myLocation = GetMylocation(arr);
    var VtriDt = GetLocationDoiThu(arr);
    var x = myLocation.col;
    var y = myLocation.row;
    var map1 = arr.map;
    var yMin = (y - r) > 1 ? (y - r) : 1;
    var yMax = (y + r) < (MAP_y - 1) ? (y + r) : (MAP_y - 1);
    var xMin = (x - r) > 1 ? (x - r) : 1;
    var xMax = (x + r) < (MAP_x - 1) ? (x + r) : (MAP_x - 1);

    var listLoc = [];
    for (let i = yMin; i <= yMax; i++) {
        for (let j = xMin; j <= xMax; j++) {
            if(map1[i][j] == emptyBox)
            {
                var point = GetPoint(arr, i, j);
                var dy = Math.abs(VtriDt.row - i);
                var dx = Math.abs(VtriDt.col - j);
                if(point > 0 && (dy > 9 || dx > 9))
                {
                    var loc = {row: i, col: j, point: point};
                    listLoc.push(loc);
                }
            }
        }
    }

    var listTarget = [];

    for (let index = 0; index < listLoc.length; index++) {
        var loc = listLoc[index];
        var str1 = MoveToLoc(arr, myLocation, loc);
        var str2 = MoveToLoc(arr, VtriDt, loc);
        var kc = str2.length - str1.length;
        if((kc >= 10 || str2.length == 0))
        {
            if(str1.length > 0)
            {
                var tarGet = {loc: loc, way: str1};
                listTarget.push(tarGet);
            }
        }
    }
    
    return listTarget;
}

function FindLocationSimple(arr) {
    var listTg = [];
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var point = GetPoint(arr, y, x);
    var locMyl = {row: y, col: x, point: point};
    var result = {loc: locMyl, way: ""};
    for (let i = 0; i < 20; i++) {
        listTg = FindListTarget(arr, 5 + i);
        if(listTg.length > 0) break;
    }
    
    if(listTg.length == 0) return result;
    result = listTg[0];
    for (let i = 1; i < listTg.length; i++) {
        if(result.way.length > listTg[i].way.length && CheckIsSafe(myl, listTg[i].loc))
        {
            result = listTg[i];
        }
    }
    return result;
}

function CheckIsSafe(loc1, loc2)
{
    if(loc1.row != loc2.row && loc1.col != loc2.col)
    {
        return true;
    }
    return false;
}

function DatBomBacVV(arr) {
    var target = FindLocationSimple(arr);
    var str = "";
    var locationRunBestter = FindWayBestterToRun(arr, target.loc, target.loc);
    if(locationRunBestter.way.length > 1 &&  target.loc.point > 0)
    {
        str = target.way + 'b' + locationRunBestter.way;
    }
    else if(target.loc.point == 0)
    {
        var attack = GetShortWay(arr);
        if(attack.length > 0) 
        {
            str = attack;
        }
    }
    return str;
}

function GetPoint(arr, y, x) {
    var map1 = arr.map;
    var d = 0;
    if(map1[y][x+1] == woodenWall) d++;
    if(map1[y][x-1] == woodenWall) d++;
    if(map1[y+1][x] == woodenWall) d++;
    if(map1[y-1][x] == woodenWall) d++;

    return d;
}

function MoveToLoc(arr, location, targetLocation) {
    var moveEatSpoils = ""
    var listMove = [];
    var up = moveUpWithTarget(arr, location, targetLocation);
    var down = moveDownWithTarget(arr, location, targetLocation);
    var right = moveRightWithTarget(arr, location, targetLocation);
    var left = moveLeftWithTarget(arr, location, targetLocation);
    
    for (let i = 0; i < up.length; i++) {
        var loc = up[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < down.length; i++) {
        var loc = down[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < right.length; i++) {
        var loc = right[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    for (let i = 0; i < left.length; i++) {
        var loc = left[i];
        if(loc.row == targetLocation.row && loc.col == targetLocation.col)
        {
            listMove.push(loc.way);
        }
    }

    if(listMove.length == 0) return "";
    moveEatSpoils = listMove[0];
    for (let i = 0; i < listMove.length; i++) {
        if(moveEatSpoils.length > listMove[i].length)
        {
            moveEatSpoils = listMove[i].length;
        }
    }
    return moveEatSpoils;
}