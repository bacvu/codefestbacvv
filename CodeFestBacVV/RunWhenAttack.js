
var LOC_DT = {
    UP_LEFT:'upLeft',
    UP_RIGHT: 'upRight',
    DOWN_RIGHT: 'downRight',
    DOWN_LEFT: 'downLeft',
}

function CheckLocationDT(arr, vtriDt) {
    var myL = GetMylocation(arr);
    var xdt = vtriDt.col;
    var ydt = vtriDt.row;
    var x = myL.col;
    var y = myL.row;
    var result = "";
    var midY = (MAP_y - 1)/ 2;
    var midX = (MAP_x - 1)/ 2;
    if(ydt < y && xdt < x)
    {
        result = LOC_DT.UP_LEFT;
    }
    else if(ydt == y && xdt < x){
        if(y >= midY)
        {
            result = LOC_DT.DOWN_LEFT;
        }
        else{
            result = LOC_DT.UP_LEFT;
        }
    }
    else if(ydt > y && xdt < x)
    {
        result = LOC_DT.DOWN_LEFT;
    }
    else if(ydt < y && xdt > x)
    {
        result = LOC_DT.UP_RIGHT;
    }
    else if(ydt == y && xdt > x){
        if(y >= midY)
        {
            result = LOC_DT.DOWN_RIGHT;
        }
        else{
            result = LOC_DT.UP_RIGHT;
        }
    }
    else if(ydt > y && xdt > x)
    {
        result = LOC_DT.DOWN_RIGHT;
    }
    else if(ydt < y && xdt == x)
    {
        if(x < midX)
        {
            result = LOC_DT.UP_LEFT;
        }
        else{
            result = LOC_DT.UP_RIGHT;
        }
    }
    else if(ydt > y && xdt == x)
    {
        if(x < midX)
        {
            result = LOC_DT.DOWN_LEFT;
        }
        else{
            result = LOC_DT.DOWN_RIGHT;
        }
    }
    else if(xdt == x && ydt == y)
    {
        if(x < midX && y < midY)
        {
            result = LOC_DT.UP_LEFT;
        }
        else if(x >=midX && y < midY)
        {
            result = LOC_DT.UP_RIGHT;
        }
        else if(x < midX && y > midY)
        {
            result = LOC_DT.DOWN_LEFT;
        }
        else{
            result = LOC_DT.DOWN_RIGHT;
        }
    }
    return result;
}

function FindEmtyBox(arr, yMin, yMax, xMin, xMax) {
    var map1 = arr.map;
    var listResult = [];
    for (let i = yMin; i <= yMax; i++) {
        for (let j = xMin; j <= xMax; j++) {
            if(map1[i][j] == emptyBox)
            {
                var loc = {row: i, col: j};
                listResult.push(loc);
            }
        }
    }
    return listResult;
}

function ListTargetRunDownRight(arr) {
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var xMax = (x + 5) < (MAP_x - 1) ?(x + 5) : (MAP_x - 1);
    var yMax = (y + 4) < (MAP_y - 1) ?(y + 4) : (MAP_y - 2);
    var listTarGet = FindEmtyBox(arr, y + 1, yMax, x + 1, xMax);
    return listTarGet;
}

function ListTargetRunDownLeft(arr) {
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var xMin = (x - 5) > 0 ?(x - 5) : 1;
    var yMax = (y + 4) < (MAP_y - 1) ?(y + 4) : (MAP_y - 2);
    var listTarGet = FindEmtyBox(arr, y + 1, yMax, xMin, x - 1);
    return listTarGet;
}

function ListTargetRunUpLeft(arr) {
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var xMin = (x - 5) > 0 ?(x - 5) : 1;
    var yMin = (y - 4) > 0 ?(y - 4) : 1;
    var listTarGet = FindEmtyBox(arr, yMin, y - 1, xMin, x - 1);
    return listTarGet;
}

function ListTargetRunUpRight(arr) {
    var myl = GetMylocation(arr);
    var x = myl.col;
    var y = myl.row;
    var xMax = (x + 5) < (MAP_x - 1) ?(x + 5) : (MAP_x - 2);
    var yMin = (y - 4) > 0 ?(y - 4) : 1;
    var listTarGet = FindEmtyBox(arr, yMin, y - 1, x + 1, xMax);
    return listTarGet;
}

function FindMoveBestter(arr, listarGet) {
    var run = {result:false, way: ""};
    if(listarGet.length == 0) return run;
    var listMov = [];
    var myl = GetMylocation(arr);
    for (let i = 0; i < listarGet.length; i++) {
        var str = MoveToLoc(arr, myl, listarGet[i]);
        if(str.length > 0 && IsSafe(arr, listarGet[i]))
        {
            listMov.push(str);
        }
    }
    if(listMov.length > 0)
    {
        var besterWay = listMov[0]; 
        for (let i = 1; i < listMov.length; i++) {
            if(besterWay.length < listMov[i].length)
            {
                besterWay = listMov[i];
            }
        }
        run = {result:true, way: besterWay};
    }
    return run;
}

function GetWayRun(arr, strloc) {
    var run = {result:false, way: ""};  
    switch (strloc) {
        case LOC_DT.UP_LEFT:
            var listDownRight = ListTargetRunDownRight(arr);
            run = FindMoveBestter(arr, listDownRight);
            break;
        case LOC_DT.UP_RIGHT:
            var listDownLeft = ListTargetRunDownLeft(arr);
            run = FindMoveBestter(arr, listDownLeft);
            break;  
        case LOC_DT.DOWN_LEFT:
            var listUpRight = ListTargetRunUpRight(arr);
            run = FindMoveBestter(arr, listUpRight);
            break; 
        case LOC_DT.DOWN_RIGHT:
            var listUpleft = ListTargetRunUpLeft(arr);
            run = FindMoveBestter(arr, listUpleft);
            break;           
        default:
            break;
    }
    MAXResIgnor = 30 - run.way.length;
    console.log("Run when attack!:", run);
    return run;
}

function RunWhenAttack(arr, loc) {
    var strLoc = CheckLocationDT(arr, loc);
    var result = GetWayRun(arr, strLoc);
    return result;
}
