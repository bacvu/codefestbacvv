
function GetListLocBomNo(arr, locbom)
{
    var listTarget = [];
    var pow = GetPowDT(arr);
    pow += 1;
    var ydt = locbom.row;
    var xdt = locbom.col;
    var yMin = (ydt - pow) > 1 ? (ydt - pow) : 1;
    var yMax = (ydt + pow) < MAP_y ? (ydt + pow) : MAP_y;
    var xMin = (xdt - pow) > 1 ? (xdt - pow) : 1;
    var xMax = (xdt + pow) < MAP_x ? (xdt + pow) : MAP_x;
    var map1 = arr.map;

    for (let i = (ydt -1); i >= yMin; i--) {
        if(map1[i][xdt] == emptyBox)
        {
            var up = {row: i, col: xdt}
            listTarget.push(up);
        }
        else{
            break;
        }
    }

    for (let i = (ydt + 1); i <= yMax; i++) {
        if(map1[i][xdt] == emptyBox)
        {
            var down = {row: i, col: xdt}
            listTarget.push(down);
        } 
        else{
            break;
        }
    }

    for (let i = (xdt -1); i >= xMin; i--) {
        if(map1[ydt][i] == emptyBox)
        {
            var left = {row: ydt, col: i}
            listTarget.push(left);
        }
        else{
            break;
        }
    }

    for (let i = (xdt + 1); i <= xMax; i++) {
        if(map1[ydt][i] == emptyBox)
        {
            var right = {row: ydt, col: i}
            listTarget.push(right);
        }
        else{
            break;
        }
    }

    return listTarget;
}

function ListBomno(arr, listBom) {
    var listResult = [];
    for (let i = 0; i < listBom.length; i++) {
        var list = GetListLocBomNo(arr, listBom[i]);
        for (let j = 0; j < list.length; j++) {
            listResult.push(list[j]);
            
        }
    }
    return listResult;
}

function AddListLocIgnor(listLoc)
{
    for (let i = 0; i < listLoc.length; i++) {
        listLocationIgnore.push(listLoc[i]);
    }
}

function RemoveListLocIgnor(listLoc)
{
    for (let i = 0; i < listLoc.length; i++) {
        listLocationIgnore.pop();
    }
}