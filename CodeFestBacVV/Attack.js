function GetLocationDoiThu(arr)
{
    var listPlayer = arr.players;
    var result = {};
    for (var i = 0; i < listPlayer.length; i++) {
        if(listPlayer[i].id != playerId)
        {
            result = listPlayer[i].currentPosition;
        }
    }
    return result;
}

function GetMyPow(arr)
{
    var listPlayer = arr.players;
    var result = 1;
    for (var i = 0; i < listPlayer.length; i++) {
        if(listPlayer[i].id == playerId)
        {
            result += listPlayer[i].powerStone;
        }
    }
    return result;
}

function GetPowDT(arr)
{
    var listPlayer = arr.players;
    var result = 1;
    for (var i = 0; i < listPlayer.length; i++) {
        if(listPlayer[i].id != playerId)
        {
            result += listPlayer[i].powerStone;
        }
    }
    return result;
}

function IsSafe(arr, myloc) {
    var y = myloc.row;
    var x = myloc.col;
    var lBoms = arr.bombs;
    var listBoms = [];
    lBoms.forEach(item => {
        if(item.remainTime > 0)
        {
            listBoms.push(item);
        }
    });
    var ok = 0;
    listBoms.forEach(item => {
        if(item.row == y || item.col == x)
        {
            if(item.row == y && IsDiedFlowY(arr, y, item.col, x))
            {
                ok++;
            }
            else if(item.col == x && IsDiedFlowX(arr, x, item.row, y)){
                ok++;
            }
        }
    });
    return ok == 0;
}

function IsDiedFlowY(arr, y, d1, d2) {
    var min = d1 < d2 ? d1 : d2;
    var max = d1 > d2 ? d1 : d2;
    var map1 = arr.map;
    var result = 0;
    for (let i = min; i <= max; i++) {
        if(map1[y][i] != emptyBox)
        {
            result++;
        }
    }
    return result == 0;
}

function IsDiedFlowX(arr, x, d1, d2) {
    var min = d1 < d2 ? d1 : d2;
    var max = d1 > d2 ? d1 : d2;
    var map1 = arr.map;
    var result = 0;
    for (let i = min; i <= max; i++) {
        if(map1[i][x] != emptyBox)
        {
            result++;
        }
    }
    return result == 0;
}

function GetListTarGetToAttack(arr)
{
    var listTarget = [];
    var pow = GetMyPow(arr);
    var vitriDoiThu = GetLocationDoiThu(arr);
    var ydt = vitriDoiThu.row;
    var xdt = vitriDoiThu.col;
    var yMin = (ydt - pow) > 1 ? (ydt - pow) : 1;
    var yMax = (ydt + pow) < MAP_y ? (ydt + pow) : MAP_y;
    var xMin = (xdt - pow) > 1 ? (xdt - pow) : 1;
    var xMax = (xdt + pow) < MAP_x ? (xdt + pow) : MAP_x;
    var map1 = arr.map;

    for (let i = (ydt -1); i >= yMin; i--) {
        if(map1[i][xdt] == emptyBox)
        {
            var up = {row: i, col: xdt}
            listTarget.push(up);
        }
        else{
            break;
        }
    }

    for (let i = (ydt + 1); i <= yMax; i++) {
        if(map1[i][xdt] == emptyBox)
        {
            var down = {row: i, col: xdt}
            listTarget.push(down);
        } 
        else{
            break;
        }
    }

    for (let i = (xdt -1); i >= xMin; i--) {
        if(map1[ydt][i] == emptyBox)
        {
            var left = {row: ydt, col: i}
            listTarget.push(left);
        }
        else{
            break;
        }
    }

    for (let i = (xdt + 1); i <= xMax; i++) {
        if(map1[ydt][i] == emptyBox)
        {
            var right = {row: ydt, col: i}
            listTarget.push(right);
        }
        else{
            break;
        }
    }

    return listTarget;

}

function GetShortWay(arr) {

    var listTarget = GetListTarGetToAttack(arr);
    var myloc = GetMylocation(arr);
    var result = "";
    var lisMove = [];
    for (let i = 0; i < listTarget.length; i++) {
        var str = MoveToLoc(arr, myloc, listTarget[i])
        if(str.length > 0)
        {
            lisMove.push(str);
        }
    }
    if(lisMove.length == 0) return result;
    result = lisMove[0];
    for (let index = 0; index < lisMove.length; index++) {
        if(result.length > lisMove[index].length)
        {
            result = lisMove[index];
        }
    }

    return result;
}

function CheckAttackBacVV(arr)
{
    var checkAttack = {result: false, way: ""};
    var str = GetShortWay(arr);
    if(str.length > 0)
    {
        checkAttack = {result: true, way: str}; 
    }
    return checkAttack;
}

var flagAttack = true;

function AttackBacVV(arr, res) {
    var check = CheckAttackBacVV(arr);
    var myL = GetMylocation(arr);
    var locDt = GetLocationDoiThu(arr);
    if(check.result || ((myL.row == locDt.row || myL.col == locDt.col) && CheckSetBoms(arr, myL)))
    {
        var listBoms = GetMyBomList(arr);
        console.log("List my boms:", listBoms)
        if(listBoms.length > 0)
        {
            if(IsSafe(arr, myL) == false)
            {
                console.log("CHUA AN TOAN");
                var bom = listBoms[0];
                var run = ThoatBoms(arr, bom);
                console.log("Chay Bom1:", run);
                socket.emit('drive player', { direction: run });
            }
            return true;
        }
        else{
            if(myL.row == locDt.row || myL.col == locDt.col)
            {
                if(CheckSetBoms(arr, myL))
                {
                    //Xử đối thủ.
                    listLocationIgnore.push(locDt);
                    var runBacVV = RunWhenAttack(arr, locDt);
                    var locationRunBestter = {};
                    if(runBacVV.result == false)
                    {
                        locationRunBestter = FindWayBestterToRun(arr, myL, myL);
                    }
                    else{
                        locationRunBestter = runBacVV;
                    }
                    var str = 'b' + locationRunBestter.way;
                    console.log("bacvv xu doi thu:", str);
                    socket.emit('drive player', { direction: str });
                    listLocationIgnore.pop();
                    flagAttack = false;
                    return true;
                }
                else if(flagAttack || CheckContinueMove(arr, res)){
                    var s = check.way;
                    console.log("di chuyen nuoc 1:", s);
                    socket.emit('drive player', { direction: s[0]});
                    flagAttack = false;
                    return true;
                }
            }
            else if(flagAttack || CheckContinueMove(arr, res)){
                var s = check.way;
                console.log("di chuyen nuoc 1 2:", s);
                socket.emit('drive player', { direction: s[0]});
                flagAttack = false;
                return true;
            }
        }
    }
    return check.result;
}

function CheckSetBoms(arr, loc) {
    var listTarget = GetListTarGetToAttack(arr);
    var ok = 0;
    listTarget.forEach(item => {
        if(item.row == loc.row && item.col == loc.col)
        {
            ok++;
        }
    });
    return ok > 0;
}

function ThoatBoms(arr, locB) {
    var vtrDt = GetLocationDoiThu(arr);
    listLocationIgnore.push(locB);
    listLocationIgnore.push(vtrDt);

    var runBestter = RunWhenAttack(arr, locB);
    if(runBestter.result)
    {
        console.log("Run bestter ok");
        listLocationIgnore.pop();
        listLocationIgnore.pop();
        return runBestter.way;
    }
    var run = FindWayBestterToRun(arr, locB, locB);
    listLocationIgnore.pop();
    listLocationIgnore.pop();
    return run.way;
}

function GetMyBomList(arr) {
    var list = arr.bombs;
    var result = [];
    for (let index = 0; index < list.length; index++) {
        if(list[index].playerId == playerId && list[index].remainTime > 0)
        {
            result.push(list[index]);
        }
    }
    return result;
}

function CheckContinueMove(arr, res) {
    if(res.tag == TICKTACK_TAG.PLAYER_STOP_MOVING && res.player_id == playerId)
    {
        return true;
    }
    else if(res.tag == TICKTACK_TAG.BOMB_EXPLOSED && res.player_id == playerId){
        return true;
    }
    else if(res.tag == TICKTACK_TAG.START_GAME)
    {
        return true;
    }
    return false;
}

// function CheckToTheBombSite(str, locDt, myL)
// {
//     var x = myL.col;
//     var y = myL.row;
//     var xDt = locDt.col;
//     var yDt = locDt.row;
//     switch (str) {
//         case '1':
//             if(y == ydt || (y-1))
//             break;
    
//         default:
//             break;
//     }
// }
