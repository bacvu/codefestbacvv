

// 1 = UpLeft
// 3 = UpRight
// 3 = DownLeft
// 4 = DownRight

function PointLocationRun(arr, locBom, y, x){
    var yb = locBom.row;
    var xb = locBom.col
    var a = Math.abs(xb - x);
    var b = Math.abs(yb - y);
    var point = -10;
    if(yb != y && xb != x){
        //point += (a + b) * 5;
        point = 0;
    }

    else if(yb == y && a <= 6 ){
        point -= (8 - a) * 20;
    }

    else if(xb == x && b <= 6 ){
        point -= (8 - b) * 20;
    }
    return point;
}

function IsSafeLocation(locBom, y, x) {
    if(y != locBom.row && x != locBom.col )
    {
        return true;
    }
    return false;
}


function RunUp(arr, locBom, loc) {
    var forLoop = 0;
    var map1 = arr.map;
    var y = loc.row;
    var x = loc.col;
    var str = "";
    var myPoint = PointLocationRun(arr, locBom, y, x);
    var way = {point:myPoint, way:"", row: y, col: x};
    var p = 0;
    var listWay = [];
    listWay.push(way);
    // Find location bestter to run
    while(str.length < 7)
    {
        if(y > 0 && x > 0 && x < (MAP_x - 1))
        {
             if(IsMoveUp(arr, y, x) || IsMoveLeft(arr, y, x) || IsMoveRight(arr, y, x))
             {
                if(IsMoveUp(arr, y, x))
                {
                    y--;
                    str += '3';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                if(IsMoveLeft(arr, y, x) && IsMoveRight(arr, y, x))
                {
                    if(UuTienLeftUp(arr, y, x) && IsMoveLeft(arr, y, x))
                    {
                        if(str[str.length - 1] == '2') break;
                        x--;
                        str += '1';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                    else if(IsMoveRight(arr, y, x)){
                        if(str[str.length - 1] == '1') break;
                        x++;
                        str += '2'
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                }

                else if(IsMoveLeft(arr, y, x))
                {
                    if(str[str.length - 1] == '2') break;
                        x--;
                        str += '1';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                }

                else if(IsMoveRight(arr, y, x))
                {
                    if(str[str.length - 1] == '1') break;
                    x++;
                    str += '2'
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
             }
             else{
                break;
             }
        }
        else{
            break;
        }
        forLoop++;
        if(forLoop >= 50) break;
    }

    //Find Way bestter Follow move up.
    var wayBestter = listWay[0];
    for (let index = 1; index < listWay.length; index++) {
        if(wayBestter.point <= listWay[index].point)
        {
            wayBestter = listWay[index];
        }
    }
    //console.log("Way bestter to runUp:", wayBestter.way);
    //socket.emit('drive player', { direction: wayBestter.way });
    return wayBestter;
}

function RunDown(arr, locBom, loc) {
    // body...
    var forLoop = 0;
    var map1 = arr.map;
    var y = loc.row;
    var x = loc.col;
    var str = "";
    var myPoint = PointLocationRun(arr, locBom, y, x);
    var way = {point:myPoint, way:"", row: y, col: x};
    var p = 0;
    var listWay = [];
    listWay.push(way);
    // Fin location dat bomb
    while(str.length < 7)
    {
        if(y > 0 && x > 0 && x < (MAP_x))
        {
             if(IsMoveDown(arr, y, x) || IsMoveLeft(arr, y, x) || IsMoveRight(arr, y, x))
             {
                if(IsMoveDown(arr, y, x))
                {
                    y++;
                    str += '4';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                
                if(IsMoveLeft(arr, y, x) && IsMoveRight(arr, y, x))
                {
                    if(UuTienLeftUp(arr, y, x))
                    {
                        if(str[str.length - 1] == '2') break;
                        x--;
                        str += '1';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                    else{
                        if(str[str.length - 1] == '1') break;
                        x++;
                        str += '2'
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                }

                else if(IsMoveLeft(arr, y, x))
                {
                    if(str[str.length - 1] == '2') break;
                        x--;
                        str += '1';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                }

                else if(IsMoveRight(arr, y, x))
                {
                    if(str[str.length - 1] == '1') break;
                    x++;
                    str += '2'
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
             }
             else{
                break;
             }
        }
        else{
            break;
        }
        forLoop++;
        if(forLoop >= 50) break;
    }

    //Find Way bestter Follow move down.
    var wayBestter = listWay[0];
    for (let index = 1; index < listWay.length; index++) {
        if(wayBestter.point <= listWay[index].point)
        {
            wayBestter = listWay[index];
        }
    }
    //console.log("Way bestter follow move down:", wayBestter.way);
    //socket.emit('drive player', { direction: wayBestter.way });
    return wayBestter;
}

function RunLeft(arr, locBom, loc) {
    // body...
    var forLoop = 0;
    var map1 = arr.map;
    var y = loc.row;
    var x = loc.col;
    var str = "";
    var myPoint = PointLocationRun(arr, locBom, y, x);
    var way = {point:myPoint, way:"", row: y, col: x};
    var p = 0;
    var listWay = [];
    listWay.push(way);
    // Find location dat bomb
    while(str.length < 7)
    {
        if(x > 0 && y > 0 && y < MAP_y)
        {
             if(IsMoveLeft(arr, y, x)|| IsMoveUp(arr, y, x) || IsMoveDown(arr, y, x))
             {
                if(IsMoveLeft(arr, y, x))
                {
                    x--;
                    str += '1';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                if(IsMoveUp(arr, y, x) && IsMoveDown(arr, y, x))
                {
                    if(UuTienUpLeft(arr, y, x))
                    {
                        if(str[str.length - 1] == '4') break;
                        y--;
                        str += '3';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                    else{
                            if(str[str.length - 1] == '3') break;
                            y++;
                            str += '4'
                            p = PointLocationRun(arr, locBom, y, x);
                            if(IsSafeLocation(locBom, y, x))
                            {
                                p += 100;
                                way = {point: p, way:str, row: y, col: x};
                                listWay.push(way);
                                break;
                            }
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                        }
                }
                else if(IsMoveUp(arr, y, x))
                {
                    if(str[str.length - 1] == '4') break;
                    y--;
                    str += '3';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                else if(IsMoveDown(arr, y, x))
                {
                    if(str[str.length - 1] == '3') break;
                    y++;
                    str += '4'
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
             }
             else{
                break;
             }
        }
        else{
            break;
        }
        forLoop++;
        if(forLoop >= 50) break;
    }

    //Find Way bestter Follow move left.
    var wayBestter = listWay[0];
    for (let index = 1; index < listWay.length; index++) {
        if(wayBestter.point <= listWay[index].point)
        {
            wayBestter = listWay[index];
        }
    }
    //console.log("Way bestter follow move left:", wayBestter.way);
    //socket.emit('drive player', { direction: wayBestter.way });
    return wayBestter;
}

function RunRight(arr, locBom, loc) {
    // body...
    var forLoop = 0;
    var map1 = arr.map;
    var y = loc.row;
    var x = loc.col;
    var str = "";
    var myPoint = PointLocationRun(arr, locBom, y, x);
    var way = {point:myPoint, way:"", row: y, col: x};
    var p = 0;
    var listWay = [];
    listWay.push(way);
    // Find location dat bomb
    while(str.length < 7)
    {
        if(x < MAP_x && y > 0 && y < MAP_y)
        {
             if(IsMoveRight(arr, y, x)|| IsMoveUp(arr, y, x) || IsMoveDown(arr, y, x))
             {
                if(IsMoveRight(arr, y, x))
                {
                    x++;
                    str += '2';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                if(IsMoveUp(arr, y, x) && IsMoveDown(arr, y, x))
                {
                    if(UuTienUpLeft(arr, y, x))
                    {
                        if(str[str.length - 1] == '4') break;
                        y--;
                        str += '3';
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                    }
                    else{
                        if(str[str.length - 1] == '3') break;
                        y++;
                        str += '4'
                        p = PointLocationRun(arr, locBom, y, x);
                        if(IsSafeLocation(locBom, y, x))
                        {
                            p += 100;
                            way = {point: p, way:str, row: y, col: x};
                            listWay.push(way);
                            break;
                        }
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        }
                }
                else if(IsMoveUp(arr, y, x))
                {
                    if(str[str.length - 1] == '4') break;
                    y--;
                    str += '3';
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
                else if(IsMoveDown(arr, y, x))
                {
                    if(str[str.length - 1] == '3') break;
                    y++;
                    str += '4'
                    p = PointLocationRun(arr, locBom, y, x);
                    if(IsSafeLocation(locBom, y, x))
                    {
                        p += 100;
                        way = {point: p, way:str, row: y, col: x};
                        listWay.push(way);
                        break;
                    }
                    way = {point: p, way:str, row: y, col: x};
                    listWay.push(way);
                }
             }
             else{
                break;
             }
        }
        else{
            break;
        }
        forLoop++;
        if(forLoop >= 50) break;
    }

    //Find Way bestter Follow move right.
    var wayBestter = listWay[0];
    for (let index = 1; index < listWay.length; index++) {
        if(wayBestter.point <= listWay[index].point)
        {
            wayBestter = listWay[index];
        }
    }
    //console.log("Way bestter follow move Right:", wayBestter.way);
    //socket.emit('drive player', { direction: wayBestter.way });
    return wayBestter;
}

function FindWayBestterToRun(arr, locBom, loc) {
    // body...
    var str = "";
    var y = loc.row;
    var x = loc.col;
    var lRunBestter = {point:0, way:str, row: y, col: x};
    
    var up = RunUp(arr, locBom, loc);
    var down = RunDown(arr, locBom, loc);
    var left = RunLeft(arr, locBom, loc);
    var right = RunRight(arr, locBom, loc);

    var list = [];
    list.push(up);
    list.push(down);
    list.push(left);
    list.push(right);

    var max = 0;
    for (let index = 0; index < list.length; index++) {
        var l = list[index].way.length;
        var point = list[index].point;
        if(max < point)
        {
            max = point;
            str = list[index].way;
            lRunBestter = list[index];
        }
    }
    //console.log("bac123:",list);
    
    return lRunBestter;

}